import { createRouter, createWebHistory } from "vue-router";
import PersonalData from '../components/PersonalData'
import Home from '../components/Home'
import Education from '../components/Education'
import ItSkills from '../components/ItSkills'
import Hobbies from '../components/Hobbies'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home 
    },
    {
        path: '/personaldata',
        name: 'personaldata',
        component: PersonalData
    },
    {
        path: '/education',
        name: 'education',
        component: Education
    },
    {
        path: '/it-skills',
        name: 'it-sklls',
        component: ItSkills
    },
    {
        path: '/hobbies',
        name: 'hobbies',
        component: Hobbies
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
